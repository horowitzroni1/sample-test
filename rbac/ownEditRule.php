<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnEditRule extends Rule
{
	public $name = 'isOwner';
	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['message']) ? $params['message']->fromUserId == $user : false;
		}
		return false;
	}
}