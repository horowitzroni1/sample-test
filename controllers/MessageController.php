<?php

namespace app\controllers;

use Yii;
use app\models\Message;
use app\models\MessageSerach;
use app\models\User;
use app\models\Department;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UnauthorizedHttpException;

/**
 * MessageController implements the CRUD actions for Message model.
 */
class MessageController extends Controller
{
    public function behaviors()
    {
        return [
		  'access' => [
				'class' => AccessControl::className(),
				'only' => ['delete','create'],
				'rules' => [
					[
						'actions' => ['delete',],
						'allow' => true,
						'roles' => ['deleteMessage'],
					],
					[
						'actions' => ['create',],
						'allow' => true,
						'roles' => ['createMessage'],
					],					
				],
			],	        
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Message models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (\Yii::$app->user->can('deleteMessage')) {
        	$template = '{view} {update} {delete}';	
        } else {
        	$template = '{view} {update}';	
        }	
        $searchModel = new MessageSerach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'template' => $template,
        ]);
    }

    /**
     * Displays a single Message model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (!\Yii::$app->user->can('whatchAllMessages') && \Yii::$app->user->can('whatchOwnMessages')){
        	if(!User::isSenderOrRciever($model))
        	throw new UnauthorizedHttpException 
		("Hey, you can't watch a message for which you are not the sender or reciever!"); 
        	
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Message model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Message();
        $users = User::getUsers();
        $departments = Department::getDepartment();
        $user = User::getCurrentUser();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'users' => $users,
                'user' => $user,
                'departments' => $departments,
            ]);
        }
    }

    /**
     * Updates an existing Message model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
 		
	if (!\Yii::$app->user->can('editMessage', ['message' => $model]))
		throw new UnauthorizedHttpException 
		('Hey, this is not your message !'); 
		
						            
        $users = User::getUsers();
        $departments = Department::getDepartment();
        $user = User::getCurrentUser();        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'users' => $users,
                'user' => $user,
                'departments' => $departments,                
            ]);
        }
    }

    /**
     * Deletes an existing Message model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Message model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Message the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Message::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}