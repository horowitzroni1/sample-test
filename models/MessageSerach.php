<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Message;

/**
 * MessageSerach represents the model behind the search form about `app\models\Message`.
 */
class MessageSerach extends Message
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fromUserId', 'toUserId', 'departmentId'], 'integer'],
            [['title', 'body'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Message::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'fromUserId' => $this->fromUserId,
            'toUserId' => $this->toUserId,
            'departmentId' => $this->departmentId,
        ]);

        if (!\Yii::$app->user->can('whatchAllMessages') && \Yii::$app->user->can('whatchOwnMessages'))
        {
 	    $query->andWhere(
	            'fromUserId = '.  Yii::$app->user->id.
	            ' OR toUserId = '.  Yii::$app->user->id
            );       	
        }
        
        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'body', $this->body]);

        return $dataProvider;
    }
}