<?php

namespace app\models;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "department".
 *
 * @property integer $id
 * @property string $name
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'department';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	public static function getDepartment()
	{
		if (!\Yii::$app->user->can('sendToAll')) {
			$user = User::findOne(Yii::$app->user->id);
			$departmentId = $user->departmentId;
			$departments = ArrayHelper::
			map(self::find()->where(['id'=>$departmentId])->all(), 'id', 'name');
		} else {
			$departments = ArrayHelper::
			map(self::find()->all(), 'id', 'name');
		}		
		return $departments;						
	}
}