<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;


class RbacController extends Controller
{
	
  	public function actionInitrule()
	{
		
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\ownDepartmentSendRule;
		$auth->add($rule);
		echo "Al OK";
	}

	public function actionAddrule()
	{

		$auth = Yii::$app->authManager;	
		$rule = $auth->getRule('ownDepartment');
		$sendToOwnDepartment = $auth->getPermission('sendToOwnDepartment');
		$sendToOwnDepartment->ruleName = $rule->name;
		$auth->add($sendToOwnDepartment);
	}
}		
	
	