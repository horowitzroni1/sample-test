<?php

namespace app\models;

use Yii;
use yii\web\UnauthorizedHttpException;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property integer $fromUserId
 * @property integer $toUserId
 * @property integer $departmentId
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'body', 'fromUserId', 'toUserId', 'departmentId'], 'required'],
            [['body'], 'string'],
            [['fromUserId', 'toUserId', 'departmentId'], 'integer'],
            [['title'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'fromUserId' => 'From User ID',
            'toUserId' => 'To User ID',
            'departmentId' => 'Department ID',
        ];
    }
    
  public function beforeSave($insert)
  {
     if (parent::beforeSave($insert)) {
 	if (!\Yii::$app->user->can('sendToAll', ['message' => $this]))
		throw new UnauthorizedHttpException 
		('You can send only to your department'); 	
         return true;
      }else {
         return false;
      }
  }   
}