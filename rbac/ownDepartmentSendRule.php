<?php
namespace app\rbac;

use yii\rbac\Rule;
use app\models\User;
use Yii; 

class ownDepartmentSendRule extends Rule
{
	public $name = 'ownDepartment';
	public function execute($user, $item, $params)
	{
		$user = User::findOne($user);
		if (!Yii::$app->user->isGuest) {
			return isset($params['message']) ? $params['message']->departmentId == $user->departmentId : false;
		}
		return false;
	}
}